angular.module('myApp.view2.services', []).
  factory('testAPIservice', function($http) {

    var ergastAPI = {};

    ergastAPI.getTest = function() {
      return $http({
        method: 'JSONP', 
        url: 'http://localhost:3000/users/userlist'
      });
    }

    return ergastAPI;
  });