'use strict';

angular.module('myApp.view2', ['ngRoute', 'myApp.view2.services'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', ['$scope', 'testAPIservice', function($scope, testAPIservice) {

    console.log("Before ergastAPIservice");

    testAPIservice.getTest().success(function (response) {
        console.log("In View2Ctrl");
        $scope.testList = response.MRDdata;
        console.log("$scope.testList");


    });


    console.log("After ergastAPIservice");


}]);