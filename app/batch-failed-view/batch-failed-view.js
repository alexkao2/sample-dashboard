'use strict';

/* Controllers */
google.load('visualization', '1', {
  packages: ['corechart']
});


angular.module('myApp.batchFailedView', ['ngRoute', 'myApp.batchFailedView.services'])


.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/batchFailedView', {
    templateUrl: 'batch-failed-view/batch-failed-view.html',
    controller: 'BatchFailedReportCtrl'
  });
}])

.controller('BatchFailedReportCtrl', ['$scope', 'ergastAPIservice', function($scope, ergastAPIservice) {

$scope.nameFilter = null;
$scope.driversList = [];

var data = google.visualization.arrayToDataTable([
        ['Year', 'Sales', 'Expenses'],
        ['2004', 1000, 400],
        ['2005', 1170, 460],
        ['2006', 660, 1120],
        ['2007', 1030, 540]
      ]);
      var options = {
        title: 'Company Performance'
      };

ergastAPIservice.getDrivers().success(function (response) {

  console.log("test"); 



  $scope.driversList = response.MRData.StandingsTable.StandingsLists[0].DriverStandings;
  // Construct graph
  var arrayToDisplay = [];
  arrayToDisplay.push(["Given Name", "Points"]);

  var arrayLength = $scope.driversList.length;

    //var gData = new google.visualization.DataTable();
  for (var i = 0; i < arrayLength; ++i) {
      

    var tmpArray = [];
    var givenName = String($scope.driversList[i].Driver.givenName);
    var points = parseInt($scope.driversList[i].points, 10);
    tmpArray.push(givenName);
    tmpArray.push(points);
    // gData.addColumn(givenName, points);

    arrayToDisplay.push(tmpArray);

  }

  console.log(arrayToDisplay);
  console.log(data);


  var dataToDisplay = google.visualization.arrayToDataTable(arrayToDisplay);
  // var options = {
  //         title: 'Graph'
  //       };

/*
          {{driver.Driver.givenName}}&nbsp;{{driver.Driver.familyName}}
        </td>
        <td>{{driver.Constructors[0].name}}</td>
        <td>{{driver.points}}</td>
*/

  var chart = new google.visualization.ColumnChart(document.getElementById('chartdiv'));

  chart.draw(dataToDisplay, options);
});






/*
$scope.driversList = [
      {
          Driver: {
              givenName: 'Sebastian',
              familyName: 'Vettel'
          },
          points: 322,
          nationality: "German",
          Constructors: [
              {name: "Red Bull"}
          ]
      },
      {
          Driver: {
          givenName: 'Fernando',
              familyName: 'Alonso'
          },
          points: 207,
          nationality: "Spanish",
          Constructors: [
              {name: "Ferrari"}
          ]
      }
    ];
    */
}]);